# JBooster Build
jbooster-build是jbooster组下所有项目通用的POM项目，用于管理常用插件和依赖的版本。

## 环境要求
- Maven3.3+
- Java8+

## 仓库地址
http://repo.jbooster.org/repository/maven-public/org/jbooster/jbooster-build/x.y.z.RELEASE/jbooster-build-x.y.z.RELEASE.pom

## 构建和发布

- 发布到本地

    ```cmd
    mvn clean install
    ```

- 发布到私服（需要在本地setting.xml文件中配置相应的服务器账号密码）  
`注意：snapshot版本可以发多次，release版本只能发一次`

    ```xml
    <servers>
        <server>
          <id>repo.jbooster.org</id>
          <username>your username</username>
          <password>your password</password>
        </server>
    </servers>
    ```
    ```cmd
    mvn clean deploy -P jbooster-repo-upload
    ```

## 引入到项目
`注意：该父POM不会引入任何实质的依赖，即使普通Java项目也可用，和spring-boot-starter-parent的理念一致`

```xml
<parent>
    <groupId>org.booster</groupId>
    <artifactId>jbooster-build</artifactId>
    <version>x.y.z.RELEASE</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>
```

## 更新版本
1. 以版本号为关键字进行全局搜索和替换，注意不要替换了错误的位置
2. 提交改动至远程仓库，并发布到私服

## 更新maven-wrapper
- 生成指定版本的maven-wrapper

    ```cmd
    mvn -N io.takari:maven:wrapper -Dmaven=x.y.z
    ```

- 替换下载地址为阿里云，提高首次下载速度

    进入文件.mvn/wrapper/maven-wrapper.properties，替换字符串
    https://repo1.maven.org/maven2 为 http://maven.aliyun.com/nexus/content/groups/public